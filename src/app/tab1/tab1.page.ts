import { Component } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(public actionSheetController: ActionSheetController) {}

  async details_publications() {
    const actionSheet = await this.actionSheetController.create({
      buttons: [{
        text: 'Reportar contenido inapropiado',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log('Delete clicked');
        }
      }, {
        text: 'Dejar de seguir',
        role: 'destructive',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
        }
      },
       {
        text: 'Ir a la publicación',
        icon: 'heart',
        handler: () => {
          console.log('Favorite clicked');
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  meGusta(like) {
    var isLike = document.getElementById(like)
    isLike.style.color = "red"
  }
} 
