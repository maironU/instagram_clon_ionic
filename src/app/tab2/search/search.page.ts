import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  data = [
    {
      id: "1",
      user: "elvisflak0",
      name: "Elvis Galvis . Seguido",
      urlImg: "https://concepto.de/wp-content/uploads/2018/08/persona-e1533759204552.jpg"
    },
    {
      id: "2",
      user: "lanena_ranger",
      name: "La Nena Ranger",
      urlImg: "https://assets.entrepreneur.com/content/3x2/2000/20181012160100-atractiva.jpeg?width=700&crop=2:1"
    },
    {
      id: "3",
      user: "brieactriz",
      name: "Brie Actriz",
      urlImg: "https://estaticos.miarevista.es/media/cache/760x570_thumb/uploads/images/article/57725155a1d4251a098bc8c0/Interior1.jpg"
    },
    {
      id: "4",
      user: "lachica_hollywood",
      name: "La Chica Hollywood . Seguido",
      urlImg: "https://st-listas.20minutos.es/images/2016-02/407242/4902015_640px.jpg?1517934603"
    },
    {
      id: "5",
      user: "andrea_peña",
      name: "Andra Peña",
      urlImg: "https://nosubmarinesdotcom.files.wordpress.com/2020/02/jqlcici.jpg?w=840"
    },
    {
      id: "6",
      user: "lubyran",
      name: "Sayuris Mira",
      urlImg: "https://s1.eestatic.com/2019/04/18/cultura/cine/Series-BBC-Actrices-Cine_391971827_120752405_1706x960.jpg"
    },
    {
      id: "7",
      user: "macefron",
      name: "Mac Efron . Seguido",
      urlImg: "https://i.ytimg.com/vi/5-URlCUrjU0/maxresdefault.jpg"
    },
    {
      id: "8",
      user: "will_smith",
      name: "Will Smith",
      urlImg: "https://st-listas.20minutos.es/images/2016-02/407634/list_640px.jpg?1455585252"
    },
    {
      id: "9",
      user: "la.roca",
      name: "Jwayne Jhonson",
      urlImg: "https://www.eleconomista.com.mx/__export/1507905034001/sites/eleconomista/img/historico/dwayne_johnson_afp.jpg_73367373.jpg"
    },
    {
      id: "10",
      user: "paul_walker",
      name: "Paul Walker .Seguido",
      urlImg: "https://static.ellahoy.es/ellahoy/fotogallery/780X0/490483/paul-walker.jpg"
    },
    {
      id: "11",
      user: "toreto",
      name: "Toreto Rodriguez",
      urlImg: "https://static.ideal.es/www/pre2017/multimedia/noticias/201509/06/media/vin_diesel.jpg"
    },
    {
      id: "12",
      user: "jackye.chan",
      name: "Jackie Chan . Seguido",
      urlImg: "https://www.tribunadesanluis.com.mx/gossip/w55frc-jackiechan/alternates/LANDSCAPE_560/JackieChan"
    },
    {
      id: "13",
      user: "veronica_orozco",
      name: "Verónica Orozco . Seguido",
      urlImg: "https://www.mariaclaralopez.com/wp-content/uploads/2019/11/Veronica-Orozco-Perfil.png"
    },
    {
      id: "14",
      user: "kim.kardashian",
      name: "Kim Kardashian",
      urlImg: "https://as01.epimg.net/us/imagenes/2019/12/10/tikitakas/1576012538_085994_1576013262_noticia_normal_recorte1.jpg"
    },
    {
      id: "15",
      user: "carly.jhonson",
      name: "Carly Jonshon",
      urlImg: "https://pbs.twimg.com/profile_images/2438975933/image_400x400.jpg"
    },
    {
      id: "16",
      user: "mario.cimarro",
      name: "Mario Cimarro .Seguido",
      urlImg: "https://upload.wikimedia.org/wikipedia/commons/f/f1/Mario_Cimarro%27s_Head_Shot.jpg"
    },
  ]

  constructor(private router: Router) { }

  ngOnInit() {
  }

  searchNoFocus(){
    var input_search = document.getElementById("input_search")
    var search = document.getElementById("search")
    search.style.justifyContent = "center"
    input_search.style.maxWidth = "80px"
  }

  searchFocus(){
    var input_search = document.getElementById("input_search")
    var search = document.getElementById("search")
    search.style.justifyContent = "start"
    input_search.style.maxWidth = "none"
    input_search.focus();
  }

  goTosearch(){
    this.router.navigate(['tabs/search'])
  }

  search(search: HTMLInputElement){
    const items = Array.from(document.querySelector('ion-list').children as HTMLCollectionOf<HTMLElement>);
    requestAnimationFrame(() => {
      items.forEach(item => {
        const shouldShow = item.textContent.toLowerCase().indexOf(search.value.toLowerCase()) > -1;
        item.style.display = shouldShow ? 'block' : 'none';
      });
    });
  }

  

}
